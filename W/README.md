2019-05-24 is from: PRB **100**, 144105 (2019), https://doi.org/10.1103/PhysRevB.100.144105

2019-10-10 is from: PRM **4**, 093802 (2020), <https://doi.org/10.1103/PhysRevMaterials.4.093802>

The only difference is that the latter is also trained to gamma surfaces, otherwise they are identical.