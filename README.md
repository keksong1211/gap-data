## GAP for radiation damage

Collection of GAP potential files and training data developed within the [acclab group at University of Helsinki](http://www.acclab.helsinki.fi/sim/).

Each folder contains subfolders with the potential files and the training structures.

The potential files can be used with [QUIP+GAP](http://www.libatoms.org/Home/Software) ([github](https://github.com/libAtoms/QUIP)), which can be compiled to run with [LAMMPS](https://lammps.sandia.gov/) ([github](https://github.com/lammps/lammps)) with [pair_style quip](https://lammps.sandia.gov/doc/pair_quip.html).



#### Potentials and references:

*  [**W**](W/2019-05-24): J. Byggmästar, A. Hamedani, K. Nordlund, and F. Djurabekova, *Machine-learning interatomic potential for radiation damage and defects in tungsten*, Phys. Rev. B **100**, 144105 (2019), https://arxiv.org/abs/1908.07330, https://doi.org/10.1103/PhysRevB.100.144105
*  [**V**](V), [**Nb**](Nb), [**Mo**](Mo), [**Ta**](Ta), [**W**](W/2019-10-10): J. Byggmästar, K. Nordlund, and F. Djurabekova, *Gaussian approximation potentials for body-centered-cubic transition metals*, Physical Review Materials, **4**, 093802 (2020), <https://arxiv.org/abs/2006.14365>, <https://doi.org/10.1103/PhysRevMaterials.4.093802>
*  [**W-Mo**](W-Mo): G. Nikoulis, J. Byggmästar, J. Kioseoglou, K. Nordlund, and F. Djurabekova, *Machine-learning interatomic potential for W-Mo alloys*, J. Phys.: Condens. Matter 33 315403 (2021), <https://doi.org/10.1088/1361-648X/ac03d1>
*  [**Si**](Si):  A. Hamedani. J. Byggmästar, F. Djurabekova, G. Alahyarizadeh, R. Ghaderi, A. Minuchehr, and K. Nordlund, *Insights into the primary radiation damage of silicon by a machine learning interatomic potential*, Materials Research Letters, **8**, 10, 364-372 (2020), <https://doi.org/10.1080/21663831.2020.1771451> **AND** A. P. Bartók, J. Kermode, N. Bernstein, and G. Csányi, Machine Learning a General-Purpose Interatomic Potential for Silicon, Phys. Rev. X 8, 041048 (2018), <https://doi.org/10.1103/PhysRevX.8.041048>
*  [**Fe**](Fe): J. Byggmästar, G. Nikoulis, A. Fellman, F. Granberg, F. Djurabekova, K. Nordlund, *Multiscale machine-learning interatomic potentials for ferromagnetic and liquid iron*, J. Phys.: Condens. Matter 34 305402 (2022), https://arxiv.org/abs/2201.10237, https://doi.org/10.1088/1361-648X/ac6f39
